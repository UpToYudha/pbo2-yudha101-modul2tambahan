/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbo3;

/**
 *
 * @author yudha
 */
public class Marketing extends Pegawai implements TBelajar{
    
    private int jamKerja;
    private String studi;
    
    public Marketing(String nama, String nip,String studi,int golongan,int jamKerja)
    {super(nama,nip,studi,golongan);
        this.studi = studi;
        this.jamKerja = jamKerja;
    }

    public int getJamKerja() {
        return jamKerja;
    }
    
    public void setJamKerja(int jamKerja) {
        this.jamKerja = jamKerja;
    }
    
    public double hitLembur(){
      int gajiLembur = 0;
        if (this.jamKerja > 8) {
            gajiLembur = (this.jamKerja - 8) * 50000;
        }
        return gajiLembur;
    
    };
    
    @Override
     public double hitungGatot() {
        return hitGajiPokok() + hitLembur();
    }

      public String isStudi() {
        return studi;
    }

    public void setStudi(String studi) {
        this.studi = studi;
    }

    
    @Override
    public String isSelesai() {
       
        return studi;
 

    }

    
 
    
}
