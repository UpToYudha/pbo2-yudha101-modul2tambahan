package pbo3;


public class Pegawai implements Pendapatan {
    
    private String nip;
    private String nama;
    private int golongan;
    private double gajiTotal;
    private String studi;
    
    public Pegawai(String nama, String nip,String studi,int golongan){
        
        this.nama = nama;
        this.nip = nip;
        this.studi = studi;
        this.golongan = golongan;
        
        
      }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getGolongan() {
        return golongan;
    }

    
    public void setGolongan(int golongan) {
        this.golongan = golongan;
    }

    public double getGajiTotal() {
        return gajiTotal;
    }

   
    public void setGajiTotal(double gajiTotal) {
        this.gajiTotal = gajiTotal;
    }
     public double hitGajiPokok(){
         double gajiPokok=0;
         if (this.golongan == 1) {
            gajiPokok = 500000;
        } else if (this.golongan == 2) {
            gajiPokok = 750000;
        } else if (this.golongan == 3) {
            gajiPokok = 10000;
        } else {
            System.out.println("Tidak Ada");
        }
        return gajiPokok;
     }
    @Override
    public double hitungGatot() {
     return 0;
    }

    // STUDI TAMBAHAN
    public String getStudi() {
        return studi;
    }

    
    public void setStudi(String studi) {
        this.studi = studi;
    }

}
