/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pbo3;


public class Manager extends Pegawai{
    
    private int JumAnak;
    private int JamKerja;
    private int Istri;
 
    //Constructor
    public Manager(String nama, String nip,String studi,int golongan,int JumAnak,int JamKerja,int istri){
    
     super(nama,nip,studi,golongan);
     
     this.JumAnak = JumAnak;
     this.JamKerja = JamKerja;
     this.Istri = istri;
     
    }

    /**
     * @return the JumAnak
     */
    public int getJumAnak() {
        return JumAnak;
    }

    /**
     * @param JumAnak the JumAnak to set
     */
    public void setJumAnak(int JumAnak) {
        this.JumAnak = JumAnak;
    }

    /**
     * @return the JamKerja
     */
    public int getJamKerja() {
        return JamKerja;
    }

    /**
     * @param JamKerja the JamKerja to set
     */
    public void setJamKerja(int JamKerja) {
        this.JamKerja = JamKerja;
    }

    /**
     * @return the Istri
     */
    public int getIstri() {
        return Istri;
    }

    /**
     * @param Istri the Istri to set
     */
    public void setIstri(int Istri) {
        this.Istri = Istri;
    }
    
   
    
    
}
